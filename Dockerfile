FROM debian:stable


RUN echo "deb http://deb.debian.org/debian stable-backports main contrib non-free # available after stretch release" | tee -a /etc/apt/sources.list
RUN apt-get update && apt-get install -y make git gcc g++ gfortran cmake autoconf automake python3
RUN apt-get install -y libtool libhdf5-dev fftw3-dev libgsl-dev  wget zlib1g-dev pkg-config
RUN ( wget https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_70_0.tar.gz ; tar zxvf boost_1_70_0.tar.gz; cd boost_1_70_0;  bash ./bootstrap.sh --prefix=/opt/boost1.70 && ./b2 && ./b2 headers && ./b2 install) && rm -fr boost_1_70_0 boost_1_70_0.tar.gz
